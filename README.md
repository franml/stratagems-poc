Stratage.ms
===========

Install:

  1. `npm install`
  2. `npm run build`

  Will build the compiled project on the folder **build/**

Develop:

  1. `npm install`
  2. `npm run start`

  Will build the project, start a server on port 3000, and watch for changes on the code.
