build_src = './app'
build_dst = './build'

module.exports =
  paths:
    src: build_src
    build: build_dst

  appjs:
    out: 'app.js'
    coffeelintOptions:
      no_backticks: { level: 'warn' }
      max_line_length: { level: 'ignore' }
      no_empty_param_list: { level: 'warn' }
      no_interpolation_in_single_quotes: { level: 'warn' }
      no_unnecessary_double_quotes: { level: 'warn' }
      non_empty_constructor_needs_parens: { level: 'warn' }
      prefer_english_operator: { level: 'warn' }

  appcss:
    out: 'app.css'
    prefix: '> 1%'
    sassOptions:
      includePaths: [
        './app/styles'
        './bower_components/slick-carousel/slick'
        './bower_components/material-design-iconic-font/scss'
      ]
      outputStyle: 'compact' # nested, expanded, compact, compressed
      errLogToConsole: true

  assets:
    out: '/assets/'
    in: [
      "#{build_src}/assets/**/*"
      './bower_components/slick-carousel/slick/ajax-loader.gif'
    ]

  fonts:
    out: '/assets/fonts'
    in: [
      "./bower_components/material-design-iconic-font/dist/fonts/*"
      "./bower_components/slick-carousel/slick/fonts/*"
    ]

  libjs:
    out: 'lib.js'
    dependencies: [
      './bower_components/angular/angular.js'
      './bower_components/angular-animate/angular-animate.js'
      './bower_components/angular-aria/angular-aria.js'
      './bower_components/angular-ipsum/dist/ipsum.min.js'
      './bower_components/angular-material/angular-material.js'
      './bower_components/angular-messages/angular-messages.js'
      './bower_components/angular-resource/angular-resource.js'
      './bower_components/angular-scroll/angular-scroll.js'
      './bower_components/angular-ui-router/release/angular-ui-router.js'
      './bower_components/jquery/dist/jquery.js'
      './bower_components/lodash/lodash.js'
      './bower_components/slick-carousel/slick/slick.js'
      './bower_components/imagesloaded/imagesloaded.pkgd.js'
      './bower_components/waypoints/lib/jquery.waypoints.js'
      './bower_components/skrollr/dist/skrollr.min.js'
      './bower_components/typed.js/dist/typed.min.js'
    ]
    minDependencies: [
      './bower_components/angular/angular.min.js'
      './bower_components/angular-animate/angular-animate.min.js'
      './bower_components/angular-aria/angular-aria.min.js'
      './bower_components/angular-ipsum/dist/ipsum.min.js'
      './bower_components/angular-material/angular-material.min.js'
      './bower_components/angular-messages/angular-messages.min.js'
      './bower_components/angular-resource/angular-resource.min.js'
      './bower_components/angular-scroll/angular-scroll.min.js'
      './bower_components/angular-ui-router/release/angular-ui-router.min.js'
      './bower_components/jquery/dist/jquery.min.js'
      './bower_components/lodash/lodash.min.js'
      './bower_components/slick-carousel/slick/slick.min.js'
      './bower_components/imagesloaded/imagesloaded.pkgd.min.js'
      './bower_components/waypoints/lib/jquery.waypoints.min.js'
      './bower_components/skrollr/dist/skrollr.min.js'
      './bower_components/typed.js/dist/typed.min.js'
    ]

  libcss:
    out: 'lib.css'
    dependencies: [
      './bower_components/angular-material/angular-material.css'
      './bower_components/slick-carousel/slick/slick.css'
    ]
    minDependencies: [
      './bower_components/angular-material/angular-material.min.css'
      './bower_components/slick-carousel/slick/slick.css'
    ]

  templates:
    out: 'templates.js'
    jadeOpts:
      pretty: true
      doctype: 'html'

    tplCacheOpts:
      standalone: true
      base: (file) -> file.relative.replace /modules\//, ''
