gulp       = require 'gulp'
concat     = require 'gulp-concat'
config     = require '../config'
cssmin     = require 'gulp-cssmin'

gulp.task 'libCSS', ->
  gulp
    .src config.libcss.dependencies
    .pipe concat config.libcss.out
    .pipe gulp.dest config.paths.build

gulp.task 'libCSS:min', ->
  gulp
    .src config.libcss.minDependencies
    .pipe concat config.libcss.out
    .pipe cssmin()
    .pipe gulp.dest config.paths.build
