gulp        = require 'gulp'
concat      = require 'gulp-concat'
coffee      = require 'gulp-coffee'
coffeelint  = require 'gulp-coffeelint'
ngAnnotate  = require 'gulp-ng-annotate'
plumber     = require 'gulp-plumber'
gutil       = require 'gulp-util'
browserSync = require 'browser-sync'
config      = require '../config'
uglify      = require 'gulp-uglify'

gulp.task 'appJS', ->
  gulp
    .src "#{ config.paths.src }/**/*.coffee"
    .pipe plumber()
    .pipe coffeelint config.appjs.coffeelintOptions
    .pipe coffeelint.reporter()
    .pipe coffee().on 'error', gutil.log
    .pipe concat config.appjs.out
    .pipe ngAnnotate()
    .pipe gulp.dest config.paths.build
    .pipe browserSync.stream()

gulp.task 'appJS:min', ->
  gulp
    .src "#{ config.paths.src }/**/*.coffee"
    .pipe coffee().on 'error', gutil.log
    .pipe concat config.appjs.out
    .pipe ngAnnotate()
    .pipe uglify()
    .pipe gulp.dest config.paths.build
