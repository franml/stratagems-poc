gulp       = require 'gulp'
concat     = require 'gulp-concat'
config     = require '../config'
uglify     = require 'gulp-uglify'

gulp.task 'libJS', ->
  gulp
    .src config.libjs.dependencies
    .pipe concat config.libjs.out
    .pipe gulp.dest config.paths.build

gulp.task 'libJS:min', ->
  gulp
    .src config.libjs.minDependencies
    .pipe concat config.libjs.out
    .pipe gulp.dest config.paths.build
