gulp = require 'gulp'
seq  = require 'gulp-sequence'

require('require-dir')('./tasks', recurse: true)

gulp.task 'build', ['appCSS', 'appJS', 'assets', 'index', 'libCSS', 'libJS', 'templates']

gulp.task 'build:min', ['appCSS:min', 'appJS:min', 'assets', 'index', 'libCSS:min', 'libJS:min', 'templates']

gulp.task 'serve', ['browserSync', 'watch']

gulp.task 'default',
  seq 'clean', 'build', 'serve'
