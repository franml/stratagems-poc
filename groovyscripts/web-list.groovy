import net.sf.json.JSONObject;

def jsonEditorOptions = JSONObject.fromObject(/
{
	disable_edit_json: true,
	disable_properties: true,
	no_additional_properties: true,
	disable_collapse: true,
	disable_array_add: true,
	disable_array_delete: true,
	disable_array_reorder: true,
	theme: "bootstrap2",
	iconlib:"fontawesome4",
	schema:
	{
		title: "Instancias",
		type: "array",
		format: "object",
		items: {
			headerTemplate: "{{self.host}}",
			type: "object",
			properties: {
				deploy : {
					type: "boolean",
					format: "checkbox"
				},
				host : {
					type: "string",
					readOnly: "true"
				},
				description : {
					type: "string",
					readOnly: "true"
				}
			}	
		}
	},
	startval: [
		{
			deploy: false,
			host: "dev.stratage.ms",
			description: "És la mateixa màquina que la de mètriques."
		}
	]
}/);