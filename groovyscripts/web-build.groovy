class Main {

    static void build() {

        /**
         if (!executeCommand("export CI=true")) {System.exit(-1);}*/

        if (!executeCommand("npm install")) {
            System.exit(-1);
        }

        if (!executeCommand("npm run build")) {
            System.exit(-1);
        }
    }

    static boolean executeCommand(String command) {

        println "> " + command;

        def tagProc = command.execute();

        tagProc.waitFor();

        if (tagProc.exitValue() != 0) {
            println "Error, ${tagProc.err.text}";
            return false;
        }

        return true;
    }

    static void main(String... args) {

        build();
    }
}
