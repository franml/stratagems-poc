import groovy.json.JsonSlurper

class Main {

    static JsonSlurper jsonSlurper = new JsonSlurper();
    static Object instances = jsonSlurper.parseText(System.getenv("instances"));
    static String workspaceDir = System.getenv("WORKSPACE");
    static String user = "root";
    static String privateKeyFile = "groovyscripts/private-key.rsa";


    static void prepareKey() {

        String command = "chmod 600 " + workspaceDir + "/" + privateKeyFile;

        if (!executeCommand(command)) {
            System.exit(-1);
        }
    }

    static void createStructureIfNotExists(instance) {

        // En caso de no existir el directorio <instance.kumoDirectory> en destino, se procede a crear la estructura por defecto.
        if (!existsRemoteDirectory(instance, instance.kumoDirectory)) {

            createRemoteDirectory(instance, instance.kumoDirectory + "/anchorOutput");
            createRemoteDirectory(instance, instance.kumoDirectory + "/prospectionQueueOutput");
            createRemoteDirectory(instance, instance.kumoDirectory + "/repositoryOutput");
            createRemoteDirectory(instance, instance.kumoDirectory + "/trimmingOutput/byDepth");
            createRemoteDirectory(instance, instance.kumoDirectory + "/trimmingOutput/bySpread");
            createRemoteDirectory(instance, instance.kumoDirectory + "/trimmingOutput/byLanguage");
            createRemoteDirectory(instance, instance.kumoDirectory + "/trimmingOutput/byMaxCrawlsPerSubdomain");
            createRemoteDirectory(instance, instance.kumoDirectory + "/seeds");
        }

        // Copia los scripts de start y stop.
        String destinationStartShell = instance.kumoDirectory + "/start-kumo.sh";

        copyFileFromWorkspace(instance, "shellscripts/start-kumo.sh", destinationStartShell);
        if (!executeRemoteCommand(instance, "chmod +x " + destinationStartShell)) {
            System.exit(-1);
        }

        String destinationStopShell = instance.kumoDirectory + "/stop-kumo.sh";

        copyFileFromWorkspace(instance, "shellscripts/stop-kumo.sh", destinationStopShell);
        if (!executeRemoteCommand(instance, "chmod +x " + destinationStopShell)) {
            System.exit(-1);
        }

        String destinationCleanOutputsShell = instance.kumoDirectory + "/clean-outputs.sh";

        copyFileFromWorkspace(instance, "shellscripts/clean-outputs.sh", destinationCleanOutputsShell);
        if (!executeRemoteCommand(instance, "chmod +x " + destinationCleanOutputsShell)) {
            System.exit(-1);
        }
    }

    static void build() {

        /*
        if (!executeCoºmmand("export CI=true")) {
            System.exit(-1);
        }
*/
        if (!executeCommand("npm install")) {
            System.exit(-1);
        }

        if (!executeCommand("npm run build")) {
            System.exit(-1);
        }
    }

    static boolean existsRemoteDirectory(Object instance, String directory) {

        String command = "ssh -i " + workspaceDir + "/" + privateKeyFile + " " +
                user + "@" + instance.host + " [ -e " + directory + " ]";

        println "> " + command;

        def tagProc = command.execute();

        tagProc.waitFor();

        if (tagProc.exitValue() == 0) {
            return true;
        }

        if (tagProc.exitValue() == 1) {
            return false;
        }

        println "Error ${tagProc.exitValue()} : ${tagProc.err.text}";

        System.exit(-1);
    }

    static void createRemoteDirectory(Object instance, String directory) {

        if (!executeRemoteCommand(instance, " mkdir -p " + directory)) {
            System.exit(-1);
        }
    }

    static void copyFileFromWorkspace(Object instance, String fromFile, String toFile) {

        println "Copying file from " + fromFile + " to " + toFile;

        String command = "scp -i " + workspaceDir + "/" + privateKeyFile + " " +
                workspaceDir + "/" + fromFile + " " + user + "@" + instance.host + ":" + toFile;

        if (!executeCommand(command)) {
            System.exit(-1);
        }
    }

    static void deploy(Object instance) {

        // Copia el ejecutable de Kumo.
        copyFileFromWorkspace(instance, "target/kumo-jar-with-dependencies.jar", instance.kumoDirectory + "/kumo.jar");
        // Copia el fichero de configuración por defecto de ejemplo.
        copyFileFromWorkspace(instance, "default-kumo.xml", instance.kumoDirectory + "/default-kumo.xml");
    }

    static boolean executeCommand(String command) {

        println "> " + command;

        def tagProc = command.execute();

        tagProc.waitFor();

        if (tagProc.exitValue() != 0) {
            println "Error, ${tagProc.err.text}";
            return false;
        }

        return true;
    }

    static boolean executeNonBlockingRemoteCommand(Object instance, String command) {

        command = "ssh -f -i " + workspaceDir + "/" + privateKeyFile + " " +
                user + "@" + instance.host + " " + command;

        return executeCommand(command);
    }

    static boolean executeRemoteCommand(Object instance, String command) {

        command = "ssh -i " + workspaceDir + "/" + privateKeyFile + " " +
                user + "@" + instance.host + " " + command;

        return executeCommand(command);
    }

    static void main(String... args) {

        boolean thereIsSomethingToDeploy = false;

        instances.deploy.each {

            if (it) {
                thereIsSomethingToDeploy = true;
            }
        }

        if (thereIsSomethingToDeploy) {

            println "\n";
            println "-------------------------------------------------------------------------------------------------";
            println "                                      Start Deploy!";
            println "-------------------------------------------------------------------------------------------------";

            prepareKey();

            instances.each {

                if (it.deploy) {

                    print "\n";
                    println "------------------------------------------------------------------------------";
                    println "Deploying to: " + it.host + " [" + it.description + "]";
                    print "\n";
                    createStructureIfNotExists(it);

                    stopKumo(it);
                    deploy(it);

                    if (it.run) {
                        startKumo(it);
                    }

                    print "\n";
                    println "Deploy to " + it.host + " Done!";
                    println "------------------------------------------------------------------------------";
                }
            }

            println "-------------------------------------------------------------------------------------------------";
            println "                                      Deploy Done";
            println "-------------------------------------------------------------------------------------------------";
        } else {
            println "-------------------------------------------------------------------------------------------------";
            println "                                      Nothing to deploy";
            println "-------------------------------------------------------------------------------------------------";
        }
    }
}
