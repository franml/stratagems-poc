angular.module 'components.typedjs', []

.directive 'typedjs', ($timeout) ->
  restrict: 'A'
  scope:
    options: '&'
  templateUrl: 'components/typedjs/typedjs.html'
  link: (scope, el, attrs, ctrl) ->
    options = scope.options()
    scope.buttonText = options.buttonText

    input = $(el).find '.typed-input'
    input.typed options.pluginOptions
