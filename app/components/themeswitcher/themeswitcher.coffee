$.fn.removeClassRegexp = (regexp) ->
  $(this).removeClass (index, css) ->
    (css.match(regexp) or []).join(' ')

angular.module 'components.themeswitcher', []

.directive 'themeswitcher', ->
  restrict: 'A'
  templateUrl: 'components/themeswitcher/themeswitcher.html'
  link: (scope, el, attrs) ->
    undefined

  controllerAs: 'ctrl'
  controller: class Themeswitcher
    backgroundElements = 'header,footer'
    textElements = 'h1,h2,h3,h4,h5,header,footer'

    ### @ngInject ###
    constructor: ($scope, @$element, $mdDialog) ->
      @$scope = $scope
      @$mdDialog = $mdDialog
      @colors = [ 'white1', 'blue3', 'blue2', 'blue7', 'blue1', 'blue10', 'blue5', 'blue4', 'blue9', 'blue11', 'blue8', 'blue6', 'green1', 'green2', 'green3', 'green4', 'purple1', 'purple2', 'purple3', 'brown1', 'brown2', 'brown3', 'brown4', 'brown5', 'brown6', 'orange1', 'pink1', 'pink2' ]
      @fonts = [ 'Montserrat', 'Bitter', 'Voces', 'Forum', 'Lato', 'Dosis', 'Titillium Web', 'Josefin Sans' ]

    change: (ev, item) ->
      @$mdDialog.show
        templateUrl: 'components/themeswitcher/themeswitcher.dialog.html'
        parent: angular.element(document.body)
        targetEvent: ev
        clickOutsideToClose: true
        locals:
          color: item
          tCtrl: @
        controller: ($scope, $mdDialog, color, tCtrl) ->
          $scope.color = color
          $scope.cancel = $mdDialog.cancel
          $scope.hide = $mdDialog.hide
          $scope.setBg = ->
            tCtrl.setBackgrounds color
            $scope.hide()
          $scope.setTxt = ->
            tCtrl.setTexts color
            $scope.hide()

    setBackgrounds: (color) ->
      $(backgroundElements)
        .removeClassRegexp /(^|\s)colortheme-bg-\S+/g
        .addClass "colortheme-bg-#{ color }"

    setTexts: (color) ->
      $(textElements)
        .removeClassRegexp /(^|\s)colortheme-text-\S+/g
        .addClass "colortheme-text-#{ color }"

    setFont: (font) ->
      $('h1,h2,h3,h4,h5,p,a,button,.logo,td')
        .removeAttr 'style'
        .css 'font-family': font
      undefined
