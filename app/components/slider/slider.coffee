angular.module 'components.slider', []

.directive 'slider', ($timeout) ->
  restrict: 'A'
  scope:
    model: '&'
  templateUrl: 'components/slider/slider.html'
  controllerAs: 'slideCtrl'
  controller: class Slider
    defaults =
      dots: false
      infinite: true
      slidesToShow: 5
      centerMode: true
      focusOnSelect: true
      slidesToScroll: 5

    ### @ngInject ###
    constructor: ($scope, $element) ->
      @scope = $scope
      @scope.items = $scope.model()
      @el = $($element)

    init: ->
      @el.css 'visibility', 'hidden'
      imagesLoaded @el, => @setSlick()

    setSlick: ->
      @el.slick defaults
      @slick = @el.slick 'getSlick'

      @el.css 'visibility', 'visible'
      @setListeners()

      @slick.setPosition 0
      @slideChanged 0

    allListeners: ->
      @el.on 'beforeChange', -> console.log 'beforeChange'
      @el.on 'afterChange', -> console.log 'afterChange'
      @el.on 'init', -> console.log 'init'
      @el.on 'reInit', -> console.log 'reInit'
      @el.on 'swipe', -> console.log 'swipe'

    setListeners: ->
      @el.on 'beforeChange', (evt, slick, next, prev) =>
        @slideChanged next

    slideChanged: (pos) ->
      current = $(@slick.$slides[pos])
      domain = current.data 'domain'
      @scope.$emit 'slider:select', domain

  link: (scope, el, attrs, ctrl) ->
    ctrl.init()
