angular.module 'app', [
  # core, third party
  'ngAria', 'ngMaterial', 'ngAnimate'
  'ngResource', 'ngMessages'
  'ui.router', 'ipsum'

  # common
  'services', 'resources', 'themes'
  'templates', 'components'

  # modules
  'layout', 'landing', 'static'
]

.config ($locationProvider, $urlRouterProvider, $httpProvider, $injector) ->
  $urlRouterProvider.otherwise ($injector) ->
    # fix infinite $digest loop - https://github.com/angular-ui/ui-router/issues/1616
    $state = $injector.get '$state'
    $state.go 'landing'

  $locationProvider.html5Mode true
  $httpProvider.defaults.headers.common['Content-Type'] = 'application/json'

.run ($rootScope) ->
  $rootScope.logged = false

  $rootScope.$on '$stateChangeStart', (ev, to, toParams, from, fromParams) ->

  $rootScope.$on '$stateChangeSuccess', (ev, to) ->
    $rootScope.bodyClass = to.data?.bodyClass or ''

  $rootScope.$on '$stateChangeError', (ev, to, toParams, from, fromParams, error) ->
    console.error error
