angular.module 'layout-static', []

.config ($stateProvider) ->
  $stateProvider
    .state 'layout-static',
      abstract: true
      onEnter: ($anchorScroll) ->
        $anchorScroll()
      views:
        '':
          templateUrl: 'layout/layout-static/layout.html'

        'header@layout-static':
          templateUrl: 'layout/layout-static/header.html'

        'footer@layout-static':
          templateUrl: 'layout/layout-static/footer.html'
