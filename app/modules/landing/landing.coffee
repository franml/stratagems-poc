angular.module 'landing', []

.config ($stateProvider) ->
  $stateProvider
    .state 'landing',
      url: ''
      data:
        bodyClass: 'landing'
      views:
        '':
          templateUrl: 'landing/landing.html'

        'header@landing':
          templateUrl: 'layout/layout-static/header.html'

        'footer@landing':
          templateUrl: 'layout/layout-static/footer.html'

        'fullpage@landing':
          templateUrl: 'landing/fullpage.html'
          controller: 'FullpageCtrl'

        'main@landing':
          templateUrl: 'landing/sections.html'
          controller: 'LandingCtrl'

.controller 'LandingCtrl', ($scope) ->
  undefined

.controller 'FullpageCtrl', ($scope) ->
  $scope.typedjsOptions =
    buttonText: 'Get Links'
    pluginOptions:
      typeSpeed: 0
      startDelay: 0
      backDelay: 3000
      loop: true
      strings: [
        "^2000 links with text in anchor <span class='code'>-anchor:example*</span>"
        "^500 links pointing to pages with status <span class='code'>-statuscode:404</span>"
        "^500 links pointing to <span class='code'>-domain:example.com</span>"
        # "^500 links that contain <span class='code'>-anchor:example*</span> ^500 and return <span class='code'>-statuscode:200</span> ^500 on <span class='code'>-domain:example.com</span>"
      ]
