angular.module 'static.products', []

.config ($stateProvider) ->
  $stateProvider
    .state 'products',
      url: '/products'
      parent: 'layout-static'
      data:
        bodyClass: 'static products'
      resolve:
        Domains: (DomainsResource) ->
          DomainsResource.get()
      views:
        'main@layout-static':
          templateUrl: 'static/products/products.html'
          controller: 'ProductsCtrl'

.controller 'ProductsCtrl', ($scope, Domains, DomainStatsResource) ->
  $scope.domains = Domains
  $scope.domainInfo = null

  getDomainStats = (id) ->
    DomainStatsResource.get domain: "stats-#{ id }.json"

  $scope.$on 'slider:select', (evt, val) ->
    evt.preventDefault?()
    $scope.domainInfo = null
    $scope.$digest()
    $scope.domainInfo = getDomainStats val
