angular.module 'static.pricing', []

.config ($stateProvider) ->
  $stateProvider
    .state 'pricing',
      url: '/pricing'
      parent: 'layout-static'
      data:
        bodyClass: 'static pricing'
      views:
        'main@layout-static':
          templateUrl: 'static/pricing/pricing.html'
          controller: 'PricingCtrl'

.controller 'PricingCtrl', ($scope) ->
