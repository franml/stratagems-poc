angular.module 'static.docs', []

.config ($stateProvider) ->
  $stateProvider
    .state 'docs',
      url: '/docs'
      parent: 'layout-static'
      data:
        bodyClass: 'static docs'
      views:
        'main@layout-static':
          templateUrl: 'static/docs/docs.html'
          controller: 'DocsCtrl'

.controller 'DocsCtrl', ($scope) ->
