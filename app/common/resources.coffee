angular.module 'resources', []

.service 'DomainsResource', ($resource) ->
  $resource '/assets/domains.json'

.service 'DomainStatsResource', ($resource) ->
  $resource '/assets/:domain', { domain: '@domain' }
