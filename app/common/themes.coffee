angular.module 'themes', []

.config ($mdThemingProvider) ->
  # http://www.google.com/design/spec/style/color.html#color-color-palette
  # https://material.angularjs.org/latest/#/Theming/01_introduction
  $mdT = $mdThemingProvider

  # $mdT.definePalette 'orangeCustom', $mdT.extendPalette 'orange',
  #   '50': 'FF0000'

  # $mdT
  #   .theme 'default'
  #     .primaryPalette 'brown'
  #     .accentPalette  'orangeCustom'
  #     .warnPalette    'red'
