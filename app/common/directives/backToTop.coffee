angular.module 'services'

.directive 'backToTop', ->
  restrict: 'A'
  link: (scope, el, attrs) ->
    el = $(el)

    $(window).scroll ->
      if $(window).scrollTop() >= $(window).height()
        el.addClass 'show'
      else
        el.removeClass 'show'

    $(el).click ->
      $('html, body').animate scrollTop: 0
