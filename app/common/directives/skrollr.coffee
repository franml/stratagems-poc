angular.module 'services'

.directive 'skrollr', ->
  restrict: 'A'
  link: (scope, el, attrs) ->
    skrollr.init()

    scope.$on '$destroy', ->
      skrollr.init().destroy()
