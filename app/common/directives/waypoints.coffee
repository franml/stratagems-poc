angular.module 'services'

.directive 'waypointMoveDown', ->
  restrict: 'A'
  link: (scope, el, attrs) ->
    $(el).on 'click', ->
      $('html, body').animate scrollTop: $(window).height()

.directive 'waypoints', ($timeout) ->
  restrict: 'A'
  link: (scope, el, attrs) ->
    waypointsArray = []
    createWaypoint = (config) -> new Waypoint(config)
    addWaypoint = (config)    -> waypointsArray.push createWaypoint config

    $timeout ->
      addWaypoint
        element: $('section.feature-slides')[0]
        offset: '50px'
        handler: (direction) ->
          if direction is 'down'
            $('header').removeClass 'transparent'
          else
            $('header').addClass 'transparent'

      addWaypoint
        element: $('section.feature-slides')[0]
        handler: (direction) ->
          if direction is 'up'
            $('.bg-fixed').removeClass 'bg2'
          else
            $('.bg-fixed').addClass 'bg2'

      addWaypoint
        element: $('section.feature-slides')[0]
        handler: (direction) ->
          this.destroy()

      $('section.feature-slides .feature').each (e) ->
        createWaypoint
          element: $(this)[0]
          offset: '70%'
          handler: (direction) ->
            $(this.element)
              .find('.feature-text,.feature-img')
              .addClass 'active'

            this.destroy()

    scope.$on '$destroy', ->
      wp.destroy() for wp in waypointsArray
